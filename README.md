# Sites Importantes #

Sites para serem acessados em ambiente de homologação, produção e documentação


## **BomBordo Passageiro**

Projeto construído para Venda, Remarcação e Cancelamento de passagens para passageiros na modalidade de Cadeiras, Suítes e Camarotes. 

Site: [passageiro.ibombordo.com.br](http://passageiro.ibombordo.com.br)

---

## **BomBordo Admin Passageiro**

Projeto construído para o gerenciamento de atividades relacionadas com a administração da operação financeira e de passagens da empresa.

Site: [passageiroadmin.ibombordo.com.br](http://passageiroadmin.ibombordo.com.br)

user: admin.ibombordo.com.br<br>
senha: admin

user: empresa@ibombordo.com.br<br>
senha: empresa

---

## API **- BomBordo Passageiro**

Projeto construído para auxiliar na integração das diferentes aplicações do projeto BomBordo Passageiro

Site: [api2.ibombordo.com.br](http://api2.ibombordo.com.br) 

---

## Redirecionador

Projeto construído para redirecionar links e acompanhar os acessos feitos por usuários que se conectaram com o link enviado,

Site: [r.ibombordo.com.br](http://r.ibombordo.com.br) 

---

## BomBordoVeículo

Projeto construído para realizar a venda de passagens para veículos.

Site: [ibombordo.com.br](http://ibombordo.com.br)

---

## Helpdesk BomBordo

Projeto construído para auxiliar no atendimento de clientes, onde um usuário de uma empresa pode ser cadastrado para responder chamados criados pelos clientes.

Site: [helpdesk.ibombordo.com.br](http://helpdesk.ibombordo.com.br)

---

## API Pagamento - Veículo

Projeto construído para auxiliar na integração das diferentes aplicações de  do projeto BomBordo Passageiro.

Site: [ibombordo.com.br](http://ibombordo.com.br)

---

## BomBordo Financeiro Passageiro

Projeto construído para controlar as vendas e repasses para as empresas cadastradas

Site: financeiro.ibombordo.com.br

---

## BomBordo Admin Veículo

Projeto construído para realizar acompanhamento das vendas de passagens para veículos, permite também cadastro de veículos 

Site: [admin.ibombordo.com.br](http://admin.ibombordo.com.br)<br>
user: financeiro@ibombordo.com.br<br>
senha: bb109361<br>

---

## BackOffice - Veículo

Projeto construído para ser auxiliar do [admin.ibombordo.com.br](http://admin.ibombordo.com.br) e realizar relarórios detalhados de vendas das passagens dos veículos e disponibilizar e/ou bloquear horarios no site ibombordo.com.br 

Site: [https://backoffice.ibombordo.com.br/](https://backoffice.ibombordo.com.br/)<br>
user: [sandy@ibombordo.com.br](mailto:sandy@ibombordo.com.br)<br>
senha: z#u6rx<br>

---

## Pagamento BomBordo

Projeto construído para realizar o aompanhamento das transações de vendas realizadas no site tanto de venda passageiro quanto de veículo e verificar o status da transação de pagamento na Cielo.

Site: [pagamento.ibombordo.com.br/](https://pagamento.ibombordo.com.br/)<br>
user:  [sandythalissa94@gmail.com](mailto:sandythalissa94@gmail.com)<br>
senha: nhm*125789 <br>
---

---

## Site Octos

Projeto construído para apresentar a empresa para os clientes, feito com wordpress.

Site: [https://octos.com.br/](https://octos.com.br/)<br>
usuário: [dev@octos.com.br](mailto:dev@octos.com.br)<br>
senha: o.O-S3nh4Dificil-O.o<br>

---

## Divulgamos

Projeto construído para apresentar a empresa para os clientes, feito com wordpress.

pesquisa.ai



## WIKI 

Projeto construído para documentar os sistemas desenvolvidos pela Octos.

Site: [https://wiki.octos.com.br/](https://wiki.octos.com.br/)


## AirFlow

https://airflow.octos.com.br/ - automatizador de rotinas (emails, notificações)

https://pass.octos.com.br/ - gerenciador de senhas

http://auth.octos.com.br/ - Single sign

ln.app.br - redirecionaor

confluence

bitbucket

https://octos.atlassian.net/wiki

Cloudflare

